#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <fcntl.h>

#include "lib.h"

#define HOST "127.0.0.1"
#define PORT 10001

char get_bit(char, int);
void print_byte(char);
void hamming_encoding(char, char*);
char reassamble(char*);
void extract_info(char*);
char hamming_decoding(char*);
void correct_error(char*, int);
void complete_codes(char*);


//TASK 4
//functie care returneaza bitul de pe o pozitie
char get_bit(char byte, int pos){

	return ((byte >> pos) & 1);
}

//functie auxiliara
void print_byte(char final){
	int i;
	for(i = 7; i >= 0; i--) {
	    if ((final >> i) & 1 )
	        printf("1");
	    else
	        printf("0");   
	}
printf("\n");
}

//functie ce codifica un caracter
void hamming_encoding(char byte, char* code){
	code[0] = 0;
	code[1] = 0;
	code[2] = 0;
	char temp = 0;
//P1
	temp = get_bit(byte, 1) ^ get_bit(byte, 3) ^ get_bit(byte, 4) ^ get_bit(byte, 6) ^ get_bit(byte, 7);

	if(temp == 1){
		code[0] |= 1 << 3;
	}
//P2
	temp = get_bit(byte, 1)^get_bit(byte, 2)^ get_bit(byte, 4) ^ get_bit(byte, 5) ^ (get_bit(byte, 7));

	if(temp == 1){
		code[0] |= 1 << 2;
	}
//D1
	temp = get_bit(byte, 7);
	if(temp == 1){
		code[0] |= 1 << 1;
	}
//P4
	temp = get_bit(byte, 0) ^ get_bit(byte, 4)^get_bit(byte, 5)^get_bit(byte, 6);

	if(temp == 1){
		code[0] |= 1;
	}
//D2
	temp = get_bit(byte, 6);
	if(temp == 1){
		code[1] |= 1 << 7;
	}
//D3
	temp = get_bit(byte, 5);
	if(temp == 1){
		code[1] |= 1 << 6;
	}
//D4
	temp = get_bit(byte, 4);
	if(temp == 1){
		code[1] |= 1 << 5;
	}
//P8
	temp = get_bit(byte, 0)^get_bit(byte, 1)^get_bit(byte, 2)^get_bit(byte, 3);
	if(temp == 1){
		code[1] |= 1 << 4;
	}
//D5
	temp = get_bit(byte, 3);
	if(temp == 1){
		code[1] |= 1 << 3;
	}
//D6
	temp = get_bit(byte, 2);
	if(temp == 1){
		code[1] |= 1 << 2;
	}
//D7
	temp = get_bit(byte, 1);
	if(temp == 1){

		code[1] |= 1 << 1;
	}

	temp = get_bit(byte, 0);
	if(temp == 1){

		code[1] |= 1;
	}
}

//functie ce reconstruieste caracterul dupa corectie
char reassamble(char* byte){
	char final = 0;

//D1
	if(get_bit(byte[0], 1))
		final |= 1 << 7;
//D2
	if(get_bit(byte[1], 7))
		final |= 1 << 6;
//D3
	if(get_bit(byte[1], 6))
		final |= 1 << 5;
//D4
	if(get_bit(byte[1], 5))
		final |= 1 << 4;
//D5
	if(get_bit(byte[1], 3))
		final |= 1 << 3;
//D6
	if(get_bit(byte[1], 2))
		final |= 1 << 2;
//D7
	if(get_bit(byte[1], 1))
		final |= 1 << 1;
//D8
	if(get_bit(byte[1], 0))
		final |= 1;
return final;
}

//functie ce decodifica un mesaj primit
void extract_info(char* goodText){
	int i = 0, j = 0;
	char info;
	char ctrl_bites;
	msg t;
	char temp[3];
	temp[0] = temp[1] = 0;

	recv_message(&t);

	for(i = 0; i < t.len - 1; i+=2){
		temp[0] = temp[1] = 0;
		memcpy(temp, t.payload + i, 2);
		ctrl_bites = hamming_decoding(temp);
		//daca bitii de control nu mi-au dat 0
		if(((int)ctrl_bites)){

			correct_error(temp, (int)ctrl_bites);
		}

		info = reassamble(temp);
		memcpy(goodText+j,&info,1);
		j++;

	}
	printf("%s\n", goodText);
}

//functie ce calculeaza bitii de control
char hamming_decoding(char* code){
	char result = 0;
	char temp = 0;

//C1
	temp = get_bit(code[0],3) ^ get_bit(code[0], 1) ^ get_bit(code[1], 7) ^ get_bit(code[1], 5) ^ get_bit(code[1], 3) ^ get_bit(code[1], 1);
	if(temp == 1){
		result |= 1;
	}
//C2
	temp = get_bit(code[0], 2) ^ get_bit(code[0], 1) ^ get_bit(code[1], 6) ^ get_bit(code[1], 5) ^ get_bit(code[1], 2) ^ get_bit(code[1], 1);
	if(temp == 1){
		result |= 1 << 1;
	}
//C4
	temp = get_bit(code[0], 0) ^ get_bit(code[1], 7) ^ get_bit(code[1], 6) ^ get_bit(code[1], 5) ^ get_bit(code[1], 0);
	if(temp == 1){
		result |= 1 << 2;
	}
//C8
	temp = get_bit(code[1], 4) ^ get_bit(code[1], 3) ^ get_bit(code[1], 2) ^ get_bit(code[1], 1) ^ get_bit(code[1], 0);
	if(temp == 1){

		result |= 1 << 3;
	}
	return result;
}

//functie ce neaga un bit de pe o pozitie
void correct_error(char* text, int pos){

	if(pos <= 4)
		text[0] ^= (1 << (4 - pos));
	else
		text[1] ^= (1 << (12 - pos));

}

//functie ce codifica un text
void complete_codes(char* text){
	int i;
	msg t;
	char code[3];

	printf("%s\n",text);
	t.len = 2*strlen(text) + 4;
	for(i = 0; i < strlen(text) + 1; i++){
		code[0] = code[1] = 0;
		hamming_encoding(text[i], code);
		memcpy(t.payload + 2*i, code, 2);
	}
	send_message(&t);
}

//functie pentru binary-search
void guess_numbers_hamming(){
	msg r;
	char txt[10];
	char info[700];
	int first = 1;
	int last = 1000;
	int middle = 500;

	sprintf(txt,"%d", middle);
	complete_codes(txt);

	recv_message(&r);

	extract_info(info);
	complete_codes("ack");

	while(strcmp(info,"success\n")){
		printf("In while\n");
		if(!strcmp(info, "bigger\n")){
			first = middle + 1;
			middle = (first + last)/2;
			sprintf(txt,"%d", middle);
			complete_codes(txt);
		}

		if(!strcmp(info,"smaller\n")){
			last = middle - 1;
			middle = (first + last)/2;
			sprintf(txt,"%d",middle);
			complete_codes(txt);
		}
		recv_message(&r);
		extract_info(info);
		complete_codes("ack");
	}
}

//-----------------------------------------------------------------------------------------------

//TASK 3

//functie ce calculeaza bitul de paritate
char compute_bits(char* text, int len){
	char sum = 0;
  	int i, j;
 	 //check sum 
  	for(i = 0; i < len; i++){
    	for(j = 0; j < 8; j++)
      		sum ^= (text[i] >> j) & 1;
  }
    return sum;
}
//functie ce trimite ACK sau NACK
void send_confirm(char* text){
	msg t;

	sprintf(t.payload,"%s",text);
	t.len = strlen(text) + 1;
	send_message(&t);
}
void send_message_crc(char* text){
	msg t, r;
	int i = 0;
//compun mesajul si il trimit
	t.payload[0] = compute_bits(text, strlen(text));
	sprintf(t.payload + 1,"%s",text);
	t.len = strlen(text) + 2;
	send_message(&t);

	printf("%s\n",t.payload + 1);
//astept ACK sau NACK
	recv_message(&r);
//verific daca lungimea mesajului pe care il primesc e 6, adica e NACK
	while(r.len == 5){
		//retrimit
		send_message(&t);
		i++;
		//astept din nou confirmare
		recv_message(&r);
	}
}

//functie de primirea a unui mesaj
msg get_message_crc(){
	msg r;
	int i = 0;
	//preiau un mesaj
	recv_message(&r);
	//cat timp e gresit, trimit NACK si preiau mesajele retrimise
	while(r.payload[0] != compute_bits(r.payload + 1, r.len - 1)) {
		printf("Mesaj %d gresit\n",i);
		i++;
		send_confirm("NACK");
		recv_message(&r);
	}
	printf("%s\n",r.payload + 1);
	//daca mesajul e bun, trimit ACK
	send_confirm("ACK");
	return r;
}

//functie pentru binary-search
void guess_number(){
	msg r;
	char txt[5];
	int first = 1;
	int last = 1000;
	int middle = 500;

	sprintf(txt,"%d", middle);
	send_message_crc(txt);
	r = get_message_crc();

	while(strcmp(r.payload + 1,"success\n")){
		if(!strcmp(r.payload + 1,"bigger\n")){
			first = middle + 1;
			middle = (first + last)/2;
			sprintf(txt,"%d", middle);
			send_message_crc(txt);
		}

		if(!strcmp(r.payload + 1,"smaller\n")){
			last = middle - 1;
			middle = (first + last)/2;
			sprintf(txt,"%d",middle);
			send_message_crc(txt);
		}
		r = get_message_crc();
	}
}
//-----------------------------------------------------------------------------------------

//TASK 1 si 2

//functie pentru trimiterea unui mesaj
int send(char* text, int type){
	msg t,r;
	int res;
	//daca tipul e 0 sau 1, trimit mesaje fara CRC
	if(type == 0 || type == 1){
		sprintf(t.payload,"%s",text);
		printf("%s\n",text);
		t.len = strlen(t.payload) + 1;
		res = send_message(&t);
	}
	//astept ACK
	if(type == 1)
		recv_message(&r);
	if (res < 0) {
		perror("[RECEIVER] Receive error. Exiting.\n");
		return -1;
	}
	return 0;
}

//functie pentru binary-search
void guess_number_simplex(int type){
	msg r, t;
	char txt[5];
	int first = 1;
	int last = 1000;
	int middle = 500;

	sprintf(txt,"%d", middle);
	send(txt, type);
	recv_message(&r);
	if(type == 1){
		sprintf(t.payload,"ACK");
		t.len = strlen(t.payload) + 1;
		send_message(&t);
	}
	printf("%s\n",r.payload);

	while(strcmp(r.payload,"success\n")){

		if(!strcmp(r.payload,"bigger\n")){
			first = middle + 1;
			middle = (first + last)/2;
			sprintf(txt,"%d", middle);
			send(txt, type);
		}

		if(!strcmp(r.payload,"smaller\n")){
			last = middle - 1;
			middle = (first + last)/2;
			sprintf(txt,"%d",middle);
			send(txt, type);
		}
		recv_message(&r);
		if(type == 1){
			sprintf(t.payload,"ACK");
			t.len = strlen(t.payload) + 1;
			send_message(&t);
	}
		printf("%s\n",r.payload);
	}
}

//functie pentru receptarea mesajelor
int get(int type){

	msg r, t;
	int res;
	//am primit un mesaj
	res = recv_message(&r);
	//daca e tipul 1, trimit inapoi ACK
	if(type == 1){
	sprintf(t.payload,"ACK");
	t.len = strlen(t.payload) + 1;
	res = send_message(&t);
	}
	printf("%s\n",r.payload);

	if (res < 0) {
		perror("[RECEIVER] Receive error. Exiting.\n");
		return -1;
	}
	return 0;
}

int main(int argc, char *argv[])
{
	msg r;
	int type;
	char info[700];
	
	printf("[RECEIVER] Starting.\n");
	init(HOST, PORT);
	
	if(argc == 1)
		type = 0;
	if(argc == 2 && !strcmp(argv[1],"ack"))
		type = 1;
	if(argc == 2 && !strcmp(argv[1],"parity"))
		type = 2;
	if(argc == 2 && !strcmp(argv[1],"hamming"))
		type = 3;
	if(type != 2 && type != 3){
		get(type);
		send("Hello", type);
		get(type);
		get(type);
		get(type);
		send("YEY", type);
		send("OK", type);
		get(type);
		guess_number_simplex(type);
		get(type);
		send("exit",type);
	}
	else if(type == 2){
		get_message_crc();
		send_message_crc("Hello");
		get_message_crc();
		get_message_crc();
		get_message_crc();
		send_message_crc("YEY");
		send_message_crc("OK");
		get_message_crc();
		guess_number();
		get_message_crc();
		send_message_crc("exit");
	}
	else
	{
		extract_info(info);

		complete_codes("ack");
		complete_codes("Hello");

		recv_message(&r);

		extract_info(info);
		complete_codes("ack");

		extract_info(info);
		complete_codes("ack");

		extract_info(info);
		complete_codes("ack");

		complete_codes("YEY");

		recv_message(&r);

		complete_codes("OK");

		recv_message(&r);

		extract_info(info);
		complete_codes("ack");

		guess_numbers_hamming();

		extract_info(info);
		complete_codes("ack");

		complete_codes("exit");
		recv_message(&r);
	}
	
	printf("[RECEIVER] Finished receiving..\n");
	return 0;
}
